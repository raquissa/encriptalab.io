# Coletivo Encripta 

O Coletivo Encripta busca trazer discussões sobre segurança na internet, privacidade, criptografia e software livre, como formas de ampliar o empoderamento e a autonomia das pessoas.

## Requisitos

* git

* jekyll

* ruby e gem

## Como rodar o projeto

Instale o Ruby e Gem:

```
$ sudo aptitude install ruby-dev gem

$ sudo gem install jekyll
```

*Obs.: Em distribuições .rpm, utilize ruby-devel.*

Faça download do projeto em sua máquina, construa-o e execute localmente:
```
$ git clone https://gitlab.com/encripta/encripta.gitlab.io.git
$ cd encripta.gitlab.io
$ jekyll build
$ jekyll serve -w
```

Agora acesse localhost:4000 em seu navegador.

## Como criar um novo post

O Jekyll suporta markdown e o "transforma" para uma página estática HTML, para criar um novo post basta criar um arquivo com a extensão .md no diretório *_posts*.
Para o nome do arquivo, utilize o padrão **ano-mes-dia-nomedopost.md**

Note que pode ser necessário executar o Jekyll novamente para o post ser carregado corretamente:
```
$ jekyll serve -w
```
Após finalizar a criação do post, faça commit de suas mudanças. _Não inclua o diretório _site_ na submissão pois ele é gerado dinamicamente.

```
$ git add _posts/*
$ git commit -m "Meu novo post"
$ git push origin master
```

Documentação útil para git: http://rogerdudler.github.io/git-guide/

## Erros comuns

1. Caso obtenha o seguinte erro ao instalar o Jekyll:
```
zlib is missing; necessary for building libxml2
```

Instale o seguinte pacote e tente novamente instalar o Jekyll:

```
$ sudo aptitude install zlib1g-dev
```

2. Caso o css não esteja carregando corretamente, altere em *index.html* que está do diretório de *_site* o caminho para encontrar o arquivo CSS:

```
<link rel="stylesheet" type="text/css" href="/css/style.css" >
```

Verificar issue: https://github.com/EncriptaTudo/EncriptaTudo.github.io/issues/2

3. Se ao acessar localhost:4000 obter uma página com layout quebrado e sem os devidos posts, verifique se o comando do jekyll foi executado no local correto.
O comando *jekyll serve -w* deve ser executado dentro do diretório */encripta.gitlab.io*
