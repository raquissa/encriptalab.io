---
layout: default
title: Encripta
---

![Logo Encripta]({{ site.path }}/images/logo_invertido.png)
&nbsp;


Quem Somos Nós?
===============

O Encripta é um coletivo que teve origem em junho de 2016 e, desde então, vem organizando atividades, palestras e oficinas sobre software livre, privacidade, liberdade e segurança na internet.

Nossos princípios e as causas por que lutamos estão descritas em nosso [Manifesto](/manifesto/). Consulte também nosso [Código de Conduta](/codigo_de_conduta), válido em todos espaços do coletivo.

Contato
-------
* email: encripta arroba riseup.net <br/>
	* [chave PGP](/chave_encripta.gpg)
	* fingerprint da chave: 707F 6994 EA88 299A D458 6AE5 2FD9 E04C 6AB7 BCDE
* grupo no telegram: [@encripta](https://t.me/encripta)
* [mastodon](https://masto.donte.com.br/@_encripta) e [twitter](https://twitter.com/_encripta): as postagens são publicadas nas duas redes (toot toot <3)
* [SoundCloud](https://soundcloud.com/coletivoencripta/): nossos podcasts estão no SoundCloud (e em nosso blog)!
* Fique sabendo de todos novos posts: inscreva-se no [nosso feed](https://encripta.org/feed.xml)
* Facebook: você não nos encontrará lá :)

Eventos
-------

| Data e horário       | Descrição      | Local          |
|:---------------------|:---------------|:---------------|
| 04 e 05 de maio | [CryptoRave 2018](https://cryptorave.org) | [Cinemateca Brasileira](http://cinemateca.gov.br/pagina/visite) |
