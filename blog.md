---
layout: default
title: Blog
---

# Blog do Encripta
[RSS]({{ site.path}}/feed.xml)

{% for post in site.posts %}

<article class='post'>
  <h2 class='post-title'>
    <a href="{{ site.path }}{{ post.url }}">
      {{ post.title }}
    </a>
  </h2>
  <div class="post-date">{{ post.date | date: "%b %-d, %Y" }}</div>
  {{ post.excerpt }}[…]
</article>

{% endfor %}

