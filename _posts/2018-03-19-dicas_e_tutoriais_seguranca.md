---
layout: post
title: "Dicas e Tutorias sobre Segurança"
date: 2018-03-19
---

Nesta página estão reunidos diversos guias sobre segurança e privacidade na internet!

## Dicas e Tutoriais Para Aprender e Pegar Macetes 
### Português

- [Guia Prática de Estratégias e Táticas Para Segurança Digital Feminista](http://feminismo.org.br/guia-pratica-de-estrategias-e-taticas-para-a-seguranca-digital-feminista/)
- [Guia de Arquivamento de Vídeos Para Ativistas](http://portugues.witness.org/tutoriais)
- [Guia Prático de Combate a Vigilância na Internet](http://www.temboinalinha.org/)
- [Protestos](http://protestos.org/)
- [Guia de AutoDefesa Digital](http://autodefesa.fluxo.info)
  - [Em pdf](http://autodefesa.fluxo.info/_static/autodefesa-digital.pdf)
  - [Para defensores de Direitos Humanos](http://autodefesa.fluxo.info/_static/dh.pdf)
- [Nudes Mais Seguros](http://www.codingrights.org/safernudes/)

### Espanhol

- [Autodefesa Ciberfeminista](http://archive.org/details/kit-de-autodefensa-ciberfeminista-uno)
- [Guía Básica de Autodefensa Digital para Celulares](http://archive.org/details/guiaautodefensatelefonos)
- [Guía de Seguridad Digital para Feministas Autogestivas](http://es.hackblossom.org/cybersecurity/)
- [Todas Las Mujeres Deberían Leer Esta Guía de Seguridad Digital Feminista](http://elpais.com/elpais/2017/05/01/tentaciones/1493672485_252723.html?id_externo_rsoc=FB_CM&utm_content=buffer64a3e&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)
- [A Las Calles Sin Medo](http://sinmiedo.com.co/seguridad.html?utm_content=bufferbb2a8&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)

### Inglês

- [Security in a Box](http://securityinabox.org/en/)
- [Surveillance Self Defense EFF](http://ssd.eff.org/)
- [Frontline](http://www.frontlinedefenders.org/en/resource-publication/workbook-security-practical-steps-human-rights-defenders-risk)
- [Data Detox](http://myshadow.org/ckeditor_assets/attachments/189/datadetoxkit_optimized_01.pdf)
- [My and My Shadow](http://myshadow.org/)
- [DIY Cybersecurity for Domestic Violence](http://hackblossom.org/domestic-violence/)
- [Gendering Surveillance](http://genderingsurveillance.internetdemocracy.in/)
- [Feminist Principles](http://feministinternet.net/en http://feministinternet.net/sites/default/files/FeministPrinciplesoftheInternetv2.0_0.pdf)
- [Holistic Security (Tactical Tech)](http://holistic-security.tacticaltech.org/)
- [Be Safe - Take Back the Teck](http://www.takebackthetech.net/be-safe)
- [Gender and Tech Resources](http://gendersec.tacticaltech.org/wiki/index.php/Main_Page)
- [Zen and the Art of Making Tech Work for You](http://gendersec.tacticaltech.org/wiki/index.php/Complete_manual_)
- [Speak Up & Stay Safe(r): A Guide to Protecting Yourself From Online Harassment](http://onlinesafety.feministfrequency.com/en/)
- [Protection Manual For lGBti DeFenDers](http://protectioninternational.org/wp-content/uploads/2012/04/LGBTI_PMD_2nd_Ed_English.pdf)
- [Information Security for Journalists](http://www.tcij.org/sites/default/files/u11/InfoSec%20for%20Journalists%20V1.3.pdf)
- [Capacitar Emergency Response Kits](http://www.capacitar.org/emergency_kits.html)
- [Surveillance Self-Defense Checklist](http://medium.com/the-intercept/surveillance-self-defense-for-journalists-ce627e332db6)
- [Getting Started with Digital Security (DIA)](http://blog.witness.org/2016/11/getting-started-digital-security/)
- [First Look at Digital Security (Access Now)](http://www.accessnow.org/a-first-look-at-digital-security/)
  - [Em pdf]((http://www.accessnow.org/cms/assets/uploads/2017/02/A-first-look-at-digital-security_DigiCopy.pdf))
- [Online Harassment Resources (Hollaback)](http://www.ihollaback.org/resources/)



## Para Configurar
### Português
- [Melhorando a Privacidade do Whatsapp](http://medium.com/@mshelton/upgrading-whatsapp-security-386c8ce496d3)
- [Email Self Defense](http://emailselfdefense.fsf.org/pt-br/)

###Inglês

- [How to Run a Rogue Government Twitter Account With an Anonymous Email Address and a Burner Phone](http://theintercept.com/2017/02/20/how-to-run-a-rogue-government-twitter-account-with-an-anonymous-email-address-and-a-burner-phone/)

## Para buscar alternativas
### Inglês // Português
- [Prism Break](http://prism-break.org/)

### Espanhol // Inglês
- [Privacy Tools](http://privacytoolsio.github.io/privacytools.io/)

## Para multiplicar

- [SaferJourno DIGITAL SECURITY RESOURCES FOR MEDIA TRAINERS](http://internews.org/sites/default/files/resources/SaferJournoGuide_2014-03-21.pdf#DigitalSecTrainersGuide.indd%3A.20214%3A385)
- [LevelUp](http://www.level-up.cc/)
- [Me and My Shadow Training Curriculum](http://myshadow.org/train)
- [How to Lead a Digital Security Workshop](http://motherboard.vice.com/en_us/article/how-to-give-a-digital-security-training)

## Vídeos e materiais de apoio

- [Vídeos da Privacy International em português](http://www.youtube.com/playlist?list=PLz3h0TjXz7gUebWP9vJHPvVNmRbvfCQgU)
- [Infiltrado Xingu Vivo](http://www.youtube.com/watch?v=mVgiYmmaOps)
- [Do Not Track](http://donottrack-doc.com)
- [Spy Merchants - Inside the Secret World of the Surveillance Industry (Al Jazeera Doc)](http://www.youtube.com/watch?v=_HA-cEMKCDs)
