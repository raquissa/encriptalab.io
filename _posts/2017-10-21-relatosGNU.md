---
layout: post
title: "Relatos sobre GNU/Linux"
---

GNU/Linux não é um bicho de sete cabeças que só pode ser domado por programadores e outros profissionais de TI. GNU/Linux é sobre Liberdade, sobre autonomia. Sendo assim, é para todos nós! Pensando em desmistificar essa questão de que os sistemas operacionais GNU/Linux servem apenas para usuários avançados, surgiu a ideia de colher depoimentos da comunidade!

Queremos saber como você conheceu o GNU e quando foi isso, por que teve interesse em usá-lo, como foi (ou está sendo) sua migração, quais dificuldades encontrou, quais mudanças mais sentiu, as coisas que você mais gosta do GNU/Linux, seus pensamentos sobre a filosofia do Software Livre, com o que você trabalha/ o que estuda (para termos uma ideia da diversidade dos usuários) e o que mais quiser falar sobre GNU/Linux!

Os depoimentos podem ser enviados via email, para encripta arroba riseup.net, ou submetidos no Sandstorm clicando [aqui](https://oasis.sandstorm.io/shared/CwkFqZ8aBiRBk22CNGRcpZONE2QviwJbE3Y_W8tbX0p).

Publicaremos neste site os depoimentos como foram enviados. Caso seja enviado por email, não publicaremos o email. Se não quiser que seu nome seja publicado, não o escreva no depoimento.

Abraços livres,
<br>
Coletivo Encripta.
