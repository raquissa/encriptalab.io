---
layout: post
title: "Chamada para Dia Internacional de Proteção de Dados Pessoais"
date: 2018-01-18
---

Diariamente milhares de dados são coletados e usados por empresas, que buscam modelar nossas (inter)ações, e por Estados, que almejam o controle sob seus cidadãos. Esta coleta é realizada majoritariamente por softwares propriétarios, cujo modo e objetivo de operação são desconhecidos por nós. Estamos, então, à mercê de zeros e uns.

Desta forma, entendendo que o acesso a dados fornece poder e, ao ser feito sem consentimento, ameaça a nossa segurança, torna-se urgente a adoção de medidas que garantam nosso direito à privicidade. Assim, pensando em ampliar o debate e disseminar o conhecimento acerca de proteção de dados e antivigilância, foi instituído, em 2006, o Dia Internacional de Proteção de Dados Pessoais (28/01). Motivado a fazer algo em prol deste dia e desta causa, o Encripta está preparando uma série de publicações sobre privacidade e segurança na internet e fazemos um convite a comunidade para somar nesse rolê!

Do dia 20 ao 25 de janeiro, estaremos recebendo de vocês artigos, tutoriais, zines, músicas e/ou outras formas de expressão que falem a respeito de privacidade e segurança na internet. Tudo isso será postado em nosso blog e divulgado por pessoas que já somaram com a gente neste projeto.

Se não sabe por onde começar, aqui vão umas ideias de temas:

* Vigilância Governamental;
* Introdução ao Software Livre;
* Senhas e Verificação em Duas Etapas;
* VPN;
* PGP;
* Mensageiros Instantâneos;

Para publicar, você pode:
* fazer um Pull Request para [gitlab.com/encripta/encripta.gitlab.io](https://gitlab.com/encripta/encripta.gitlab.io) 
ou
* enviar seu texto para encripta arroba riseup ponto net

Faça texto, faça arte, se junte a nós nessa luta! Se a vigilância é centralizada, a resistência será distribuída!
