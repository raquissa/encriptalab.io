---
layout: default
title: Arquivos
---

Arquivos
========

- [Apresentação de Tatuí](/tatui.pdf)

- [Código de Conduta](/codigo_de_conduta)

- [Manifesto](/manifesto)

- Primeira edição do podcast [Papo Criptogravado](/anexos/papo_criptogravado-00.mp3)


